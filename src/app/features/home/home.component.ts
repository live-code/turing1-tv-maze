import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Series, Show } from '../../model/series';

@Component({
  selector: 'tur-home',
  template: `
    <tur-tvmaze-search (search)="search($event)"></tur-tvmaze-search>
    <tur-tvmaze-list [data]="result" (clickThumbSeries)="showDetails($event)"></tur-tvmaze-list>
    <tur-tvmaze-modal *ngIf="selectedShow" [show]="selectedShow" (close)="selectedShow = null"></tur-tvmaze-modal>
  `,
})
export class HomeComponent  {
  result: Series[] = [];
  selectedShow: Show | null = null;

  constructor(private http: HttpClient) {
    setTimeout(() => {
      this.search('soprano')
    }, 500)
  }

  search(text: string) {
    if (text.length > 0) {
      this.http.get<Series[]>(`http://api.tvmaze.com/search/shows?q=${text}`)
        .subscribe(res => {
          this.result = res;
        })
    }
  }

  showDetails(show: Show) {
    this.selectedShow = show;
  }
}


