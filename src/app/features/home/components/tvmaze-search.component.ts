import { Component, EventEmitter, OnInit, Output } from '@angular/core';

@Component({
  selector: 'tur-tvmaze-search',
  template: `
    <!--search-->
    <input
      type="text" placeholder="Search TVSeries"
      #inputRef
      (keyup.enter)="search.emit(inputRef.value)"
    >
  `,
  styles: [
  ]
})
export class TvmazeSearchComponent {
  @Output() search = new EventEmitter<string>()

}
