import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Series, Show } from '../../../model/series';

@Component({
  selector: 'tur-tvmaze-list-item',
  template: `
    <div  class="grid-item" >
      <div class="movie" (click)="showInfo = true">
        
        <div *ngIf="!showInfo">
          <img *ngIf="series.show.image" [src]="series.show.image.medium" width="100">
          <div *ngIf="!series.show.image" class="noImage"></div>
          <div class="movieText">{{series.show.name}}</div>
        </div>
        <div *ngIf="showInfo" (click)="hideInfo($event)">
          {{series.show.summary}}
        </div>
      </div>
      <button (click)="clickThumbSeries.emit(series.show)">Open Details</button>
    </div>
  `,
  styles: [`


    .grid-item {
      flex-grow: 0;
    }

    .movie {
      width: 100px;
      text-align: center;
      margin: 10px;
      cursor: pointer;
    }

    .movie img{
      width: 100%
    }

    .noImage {
      width: 100%;
      height: 140px;
      border: 1px solid #ccc;
      background: repeating-linear-gradient(
          -55deg,
          #222,
          #222 10px,
          #333 10px,
          #333 20px
      );
    }

    .movieText {
      white-space: nowrap;
      overflow: hidden;
      text-overflow: ellipsis;
      text-align: center;
    }

  `]
})
export class TvmazeListItemComponent {
  @Input() series!: Series;
  @Output() clickThumbSeries = new EventEmitter<Show>()
  showInfo: boolean = false;

  hideInfo(e: MouseEvent) {
    e.stopPropagation()
    this.showInfo = false
  }
}
