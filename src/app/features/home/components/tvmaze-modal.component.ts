import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Show } from '../../../model/series';

@Component({
  selector: 'tur-tvmaze-modal',
  template: `
    <!--series modal-->
    <h1>MODAL</h1>
    <div class="wrapper">
      <div class="content">
        <h1>{{show?.name}}</h1>
        <div *ngIf="!show?.summary">no content</div>
        <img *ngIf="show?.image" [src]="show?.image?.original" width="100%">
        <div class="tag" *ngFor="let gen of show?.genres">{{gen}}</div>
        <div [innerHTML]="show?.summary">xxxx</div>
      </div>
      <a [href]="show?.url" target="_blank" class="button">Visit website</a>
      <div class="closeButton" (click)="close.emit()">❌</div>
    </div>
  `,
  styleUrls: [`./tvmaze-modal.component.css`]
})
export class TvmazeModalComponent {
  @Input() show: Show | null = null;
  @Output() close = new EventEmitter()
}
