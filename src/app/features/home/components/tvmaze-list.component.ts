import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Series, Show } from '../../../model/series';

@Component({
  selector: 'tur-tvmaze-list',
  template: `
    <!--series result -->
    <div class="grid">
      <tur-tvmaze-list-item 
        *ngFor="let series of data" 
        [series]="series"
        (clickThumbSeries)="clickThumbSeries.emit($event)"
      >
      </tur-tvmaze-list-item>
    </div>
  `,
  styleUrls: ['./tvmaze-list.component.css']
})
export class TvmazeListComponent {
  @Input() data: Series[] = [];
  @Output() clickThumbSeries = new EventEmitter<Show>()
}
