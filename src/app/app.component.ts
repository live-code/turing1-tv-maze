import { Component } from '@angular/core';

@Component({
  selector: 'tur-root',
  template: `
    <tur-home></tur-home>
  `,
  styles: []
})
export class AppComponent {
  title = 'turing-tvmaze';
}
