import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { HomeComponent } from './features/home/home.component';
import { HttpClientModule } from '@angular/common/http';
import { TvmazeSearchComponent } from './features/home/components/tvmaze-search.component';
import { TvmazeListComponent } from './features/home/components/tvmaze-list.component';
import { TvmazeModalComponent } from './features/home/components/tvmaze-modal.component';
import { TvmazeListItemComponent } from './features/home/components/tvmaze-list-item.component';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    TvmazeSearchComponent,
    TvmazeListComponent,
    TvmazeModalComponent,
    TvmazeListItemComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
